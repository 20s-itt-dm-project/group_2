# Make it useful goals


### This is the goals of "phase 3" make it useful.
---

The goals stated here, is for the user in mind. 

- Setup guide including. 
  - How to get started.
  - How to connect the Boron to solar panel (in this case a battery)  
  - How to see the Boron is operating and online.
---
  
- User guide with documentation including.
  - How to see the dashboard from a smartphone and PC. 
---  

- Troubleshooting guide.
  - "Whatever can happen" 
  - European style, not retardo safe. 

#### The team 
---
   
* Jonas Offersen 
* Jacob Moustgaard Pløger 
* Thomas Rasmussen 
* Mikkel Krogh Hansen 


