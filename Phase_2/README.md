# Welcome to group 2 Phase_2 readme project (20S ITT2 Project)
---
This readme is about the project for 2nd semester students on UCL IT-teknology. 


# Overview
---
The main goal is to have a system where humidity sensors data is sent to node-red. 
Data is presented on a dashboard and shown with a graph on a screen. 

For more information and guides to set it up please look at the [Wiki]  

![overview](https://gitlab.com/20s-itt-dm-project/group_2/-/raw/master/Phase_2/Proof_of_concept_design/Blockdiagram_IOT_greenhouse.png?inline=false)

See the link for visual overview.  
  

# The main goals of our project 

* DUT: Device under test, these are the humidity sensors, connected to the Boron board.

* Humidity sensors

* Particle Boron: The embedded system to run the sensor software and to be the interface WIFI connection to the raspberry pi   

* Computer: Hosts the particle workbench IDE (Visual studio code)  

* Raspberry Pi: A small hardened linux system hosting a dashboard and relevant programs to upload/download data from the Boron and to/from the APIs. Configuration must be reproducable with appropriate security implemented.

# Particle Boron 

Link overview of the greenhouse prototype 

![Greenhouse](https://gitlab.com/20s-itt-dm-project/group_2/-/raw/master/Phase_2/Proof_of_concept_design/prototype_greenhouse.png)

# Organization
  
    • Identification of the steering committee
	- Jacob
	- Jonas
	- Mikkel
	- Thomas  
    • Identification of project managers
	- Nikolaj
	- Morten
   
