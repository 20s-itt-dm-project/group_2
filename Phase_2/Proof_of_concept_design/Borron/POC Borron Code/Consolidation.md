# Consolidation goals of group 2 
---

###This is our "tentative" consolidation for gorup 2
---

We are aiming at the following goals. 


* Have a working sensor with humidity  []
* Have a working sensor with temperature  []
* Have a diagram of the wiring between the components inside the greenhouse []

* Have the code running with node-red []
* Rearrange the code so it is more intuitive []

* Have node-red dashboard showing minimum temp/humid []
* Have node-red dashboard showing avg temp/humid []
* have node-red dashboard showing max temp/humid []

Please add more as we progress. 



