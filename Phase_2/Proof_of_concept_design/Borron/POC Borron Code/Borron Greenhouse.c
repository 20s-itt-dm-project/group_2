// This #include statement was automatically added by the Particle IDE.
#include <Adafruit_DHT.h>

// This #include statement was automatically added by the Particle IDE.
#include <Adafruit_DHT.h>

#include "Adafruit_DHT.h"

// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

#define DHTPIN 2     // what pin we're connected to

// Uncomment whatever type you're using!
#define DHTTYPE DHT11		// DHT 11 
//#define DHTTYPE DHT22		// DHT 22 (AM2302)
//#define DHTTYPE DHT21		// DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

//int temp = (t);

DHT dht(DHTPIN, DHTTYPE);

SYSTEM_MODE(AUTOMATIC);

char tempString[30] = "";
char humString[30] = "";

void setup() {
//	Serial.begin(115200); 
//	Serial.println("DHTxx test!");

	dht.begin();
	
//Particle.variable("temp", temp);
}

void loop() {
// Wait a few seconds between measurements.
   
    
	delay(2500);

// Reading temperature or humidity takes about 250 milliseconds!
// Sensor readings may also be up to 2 seconds 'old' (its a 
// very slow sensor)
	float hum = dht.getHumidity();
// Read temperature as Celsius
	float temp = dht.getTempCelcius();
	
	//int temp = (t);
	
	sprintf(tempString, "%.2f", temp);
    sprintf(humString, "%.2f", hum);
  
// Check if any reads failed and exit early (to try again).
	 //if (isnan(h) || isnan(t) || isnan(f)) {
	if (isnan(hum)) {
		Serial.println("Failed to read from DHT sensor!");
		return;
	}

    Particle.publish("temp", tempString, PRIVATE);
    Particle.publish("hum", humString, PRIVATE);
// Compute heat index
// Must send in temp in Fahrenheit!
	float hi = dht.getHeatIndex();
	float dp = dht.getDewPoint();
	float k = dht.getTempKelvin();

//	Serial.print("Humid: "); 
//	Serial.print(h);
//	Serial.print("% - ");
//	Serial.print("Temp: "); 
//	Serial.print(t);
//	Serial.print("*C ");
//	Serial.print(f);
//	Serial.print("*F ");
//  Serial.print(k);
//	Serial.print("*K - ");
//	Serial.print("DewP: ");
//	Serial.print(dp);
//	Serial.print("*C - ");
//	Serial.print("HeatI: ");
//	Serial.print(hi);
//	Serial.println("*C");
//	Serial.println(Time.timeStr());
}