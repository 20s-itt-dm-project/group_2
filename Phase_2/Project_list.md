Project name: IOT Greenhouse

Group name: Group 2

Checklist
------------

- [ ] Greenhouse materials, documented and pictures 
- [ ] Minimal circuit schematic
- [ ] Boron minimal system code
- [x] Boron to Rasperry Pi serial communication over UART working
- [ ] Raspberry pi configuration documented
- [x] Raspberry pi and node-red setup
- [ ] Node red showing showing dashboard with a humiditygraph 
- [ ] Hardware list (documented)  

