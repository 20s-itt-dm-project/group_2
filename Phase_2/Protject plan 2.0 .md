---
# 20S ITT2 Project
subtitle: Project plan, part II

---

# Background

This is the semester project part 2 for ITT2 where you will work with different projects, initated by a company. This project plan will cover the project, and will match topics that are taught in parallel classes.


# Purpose

The main goal is to have a system where sensor data is sent to a cloud server, presented and collected, and data and control signal may be sent back.  
This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation.

For simplicity, the goals stated will evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

The overall system that is going to be build looks as follows:  
(Insert picture of Block diargram)

//Explian the different parts of the block diagram:
* DUT: Device under test, these are the pumps for water
* Humidity sensors: These are sensor of different kinds that we are to implement into the system. Initially we will work with physical sensor simulations (analog/digital).  
* Particle Boron: The embedded system to run the sensor software and to be the interface to the serial connection to the raspberry pi and the gateway to particle device cloud.  
* Computer: Hosts the particle workbench IDE (Visual studio code)  
* Raspberry Pi: A small hardened linux system hosting a dashboard and relevant programs to upload/download data from the Boron and to/from the APIs. Configuration must be reproducable with appropriate security implemented.


Project deliveries are:  

* A system reading and writing data to and from the sensors/actuators  
* A REST API exposing the values of the sensors and option of setting and applying the actuator values.  
* Docmentation of all parts of the solution  
* Regular meeting with project stakeholders.  
* Final evaluation  

The first part of the project will be divided into 3 phases and gitlab will be used as the project management platform.


# Schedule

The project is divided into three phases from week 16 to week 30.

See the [lecture plan](https://eal-itt.gitlab.io/20s-itt2-project/20S_ITT2_lecture_plan.pdf) for details.


# Organization
  
    • Identification of the steering committee
	- Jacob
	- Jonas
	- Mikkel
	- Thomas  
    • Identification of project managers
	- Nikolaj
	- Morten
    • Composition of the project groups
	- Us
    • Identification of external resource persons or groups  


# Budget and resources

Small monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute,


# Risk assessment

1.  People not showing up  
2.  One takes control of the project and does all the work  
3.  People not doing their part  

Solutions:  
1. Ask why, and if they can do work from home.
2. Assign less issues to that person, and have a chat about it
3. Ask them if they need help

# Stakeholders

Internal:
- Our group
- Teachers


External:


Active:
- Our group


Passive:
- Teachers

   
# Communication


Main communication:
- Riot
	- Thomas: @sambakongen:matrix.org
	- Jacob: @jacobploeger:matrix.org
	- Mikkel: @losrapitos:matrix.org
	- Jonas: @jonas_offersen:matrix.org
- E-mail:
	- Thomas: thom37q6@edu.ucl.dk
	- Jacob: Jaco223r@edu.ucl.dk
	- Mikkel: mikk744e@edu.ucl.dk
	- Jonas: Jona415t@edu.ucl.dk
- Google Hangout
    - [Group_2_Hangout](https://meet.google.com/uuf-gvpr-nxh?fbclid=IwAR2GPDouPFE9qTw1TpSUmadeNxM7myz2Q12mJo3uBhro3UdpoILweoVR7ks&authuser=1) 
	
# Perspectives

This project will serve as a template for other similar projects in future semesters.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

[Evaluation is about how to gauge is the project was successful. This includes both the process and the end result. Both of which may have consequences on future projects.  
The will be some documentation needed at the end (or during) the project, e.g. external funding will require some sort of reporting after the project has finished. This must be described.]  

TBD: Students fill out this one also

# References

[Include any relevant references to legal documents, literature, books, home pages or other material that is relevant for the reader.]

TBD: Students may add stuff here at their discretion

* particle.io

  Particle.io is the IoT system we use. See [https://www.particle.io/](https://www.particle.io/)


* Common project group on gitlab.com

  The project is managed using gitlab.com. The groups is at [https://gitlab.com/20s-itt-dm-project](https://gitlab.com/20s-itt-dm-project)
