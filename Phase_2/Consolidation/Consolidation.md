# Consolidation goals of group 2 
---

### This is our "tentative" consolidation for group 2
---

We are aiming at the following goals. 


- [x] Have a working sensor with humidity 
- [x] Have a working sensor with temperature  
- [ ] Have a diagram of the wiring between the components inside the greenhouse 

- [x] Have the code running with node-red 
- [ ] Use less data, by rewriting the code

- [x] Have node-red dashboard showing minimum temp/humid 
- [x] Have node-red dashboard showing avg temp/humid 
- [x] have node-red dashboard showing max temp/humid 

 



