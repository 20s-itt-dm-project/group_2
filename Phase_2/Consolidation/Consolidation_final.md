### Consolidation plan 
---

* We will have a temp and humid sensor connected to the particle cloud, node-red and then the IBM cloud dashboard service.

* We will have a greenhouse prototype, with wires installed.

* A battery will represent the solarpanel. (The Boron have a battery pack included)

---

##### Group 2 

* Jonas Offersen

* Jacob Moustgaard Pløger 

* Thomas Rasmussen 

* Mikkel Krogh Hansen 