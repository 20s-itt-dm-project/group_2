# Welcome to group 2 readme project (20S ITT2 Project)

---
## This readme is about the project for 2nd semester students on UCL IT-teknology. 


# PHASE 1
---
## The main goal of phase 1 is to have a system where sensor data is sent to a cloud server. 
Data presented and collected and control signal may be sent back.

![overview](https://gitlab.com/20s-itt-dm-project/group_2/-/raw/master/Phase_1/Research%20and%20Documentation/docs_photos_project_overview_no_mesh.png?inline=false)

# PHASE 2
---
## The main goal of phase 2 is to have a system where sensors measure temperature and humidity. 
Data is presented on a dashboard and shown with a graph on a screen. 

![overview](https://gitlab.com/20s-itt-dm-project/group_2/-/raw/master/Phase_2/Proof_of_concept_design/Blockdiagram_IOT_greenhouse_2.png?inline=false)

![Greenhouse](https://gitlab.com/20s-itt-dm-project/group_2/-/raw/master/Phase_2/Proof_of_concept_design/prototype_greenhouse.png)


##### For more information and guides to set it up please look at the [Wiki]

# Organization
  
    • Identification of the steering committee
	- Jacob
	- Jonas
	- Mikkel
	- Thomas  
    • Identification of project managers
	- Nikolaj
	- Morten
    • Composition of the project groups
	- Computer sience
    • Identification of external resource persons or groups  
	- Hydac A/S
  