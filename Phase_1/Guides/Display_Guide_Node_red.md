## Guide to make a dashboard in Node-Red  

1. Make sure to have Node-red setup. For help look [here](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Guides/Node-red.md).
2. Make sure the two way communcation is working aswell. For help look [here](https://gitlab.com/20s-itt-dm-project/group_2/-/edit/master/Guides/Node_red_Two_way.md).
3. Watch this [video](https://www.youtube.com/watch?v=X8ustpkAJ-U&feature=youtu.be) for guidence.
