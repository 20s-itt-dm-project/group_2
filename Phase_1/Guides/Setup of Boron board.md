Set up the Boron bord: 
1. downloadet Particle IO app on appstore
2. plug the boron bord to power 
3. connect the Boron board to the phone and start setting it up through the setup process
4. after setup process is done, try flashing a program on to the bord on piticle.oi 
and sign in with the accunt you created in the setup process

For more help look here:
- [Link to guides](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Guides/Links_to_boron_guides.md)
- [Information](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Guides/Boron.md)