**Guide for two-way communication using Node-red**

1. Make sure you have set up the serial communication between RPi and Boron if you haven't you can follow this [guide](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Guides/Guide_to_two_way.md).
2. Make sure you also have an undersatnding off Node-red, for help follow this [guide](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Guides/Node-red.md).  
3. To make the communication work you can follow this [guide](https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_boron_serial).  