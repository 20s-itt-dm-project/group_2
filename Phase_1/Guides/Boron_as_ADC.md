**Boron as ADC:**

1. Make sure to have established two-way communication with node-red. for help look [here](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Guides/Node_red_Two_way.md).
2. Have a module, with two buttons and two LEDs, from which you want to meassure.
3. Connect all the hardware. Remember common ground.
4. Start up node-red on your Raspberry pi.
5. Set up the node-red following this [toturiel](https://eal-itt.gitlab.io/iot-with-particle/dashboard/node_red_dashboard).
6. Now to code your Boron. Flash this [code](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Coding/LED_Button_Boron.cpp) onto your boron and if everything is set up curretly it should work.