Guide to Node-red

1. Make sure your pi has internet connection  
2. Run the command "sudo apt-get install build-essential"
3. You have to chose the command for Raspian, to have it working on Rapsberrypi
4. Run the command "bash <(curl -sL https://raw.githubusercontent.com/node-red
/linux-installers/master/deb/update-nodejs-and-nodered)"  
This might take some time so be patient  
5. To test it use the command "node-red-pi --max-old-space-size=256"  
6. If you are using the browser on the Pi desktop, you can open the address: http://localhost:1880. If you want to use it on your monitor type in http://"RPi ip adress":1880 in your browser  (to find it you can type in "hostname -I")
7. To learn the program follow these tutorials 1. [tutorial](https://nodered.org/docs/tutorials/first-flow) 2. [tutorial](https://nodered.org/docs/tutorials/second-flow)  