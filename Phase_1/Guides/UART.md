IMPORTANT NOTE: 
You are likely in a race condition with the system. I ran into a similar issue   
when I installed a package that updated my serial settings.  

Raspi-config settings for your serial port.  

Sudo Raspi-config —> Interfacing options —> serial —> no —> yes
Reboot

THEN, do the following. 

Setup UART RPi to Boron communication  
  
1. Start with git clone on the repository of the following link:

https://gitlab.com/moozer/python-serial-generic

2. Log onto your RPi and do as follows:

sudo apt install virtualenv  
sudo apt install socat  

3. Setup the virtualenv as following  
virtualenv -p python3 venv  
source venv/bin/activate  
pip3 install -r requirements.txt  

4. test pipelines  
Go to the test directory in the python-serial-generic directory  
and type in 
'socat -d -d pty,raw,echo=0,link=./fake_device pty,raw,echo=0,link=client'  

In Terminal 1 do as follows for testing:

./fake_device_conn.py

In Terminal 2(open another) do as follows:  

python3 simple_connection.py  

In Terminal 1 do as follows for testing:  

./fake_device_send.py  

In Terminal 2 do as follows:  

python3 simple_receive.py  

5. edit boot config.txt  
sudo nano /boot/config.txt  
put in the line enable_uart=1 at the bottom  
add another line core_freq=250  
sudo reboot the RPi afterwards  

6. Setup RPi using GPIOs  
https://spellfoundry.com/2016/05/29/configuring-gpio-serial-port-raspbian-jessie-including-pi-3-4/  
Further information for RPI UART is found here:  
https://www.raspberrypi.org/documentation/configuration/uart.md  

7. Changing physical interface to run Serial.  
sudo nano simple_receive.py  
find the dictionary where it says test/client and replace it with /dev/ttyS0, to enable serial connection.  

8. make a diagram for wire setup  
https://docs.particle.io/assets/images/boron/boron-pinout-v1.0.pdf  
Find pinout for whichever RPI youve got.  
Connect the UART RX on the pi to the TX on the Boron  
Connect the UART TX on the pi to the RX on the Boron  
Connect the Ground on the pi to the Ground on the Boron  

9. Using Exercises 1 + 2, now upload the correct code + program to the Boron using the codes from:  

https://community.particle.io/t/serial-tutorial/26946  
hint: scroll to 'communicating with arduino' and look for 'photon code'.  
Connect the wires as made from your diagram.  

10. run the following code to make sure it works
'sudo python3 simple_receive.py'  

if the code won't run because you can't import the serial module  
use 'pip install pyserial'  


For extra help look at this [website](https://spellfoundry.com/2016/05/29/configuring-gpio-serial-port-raspbian-jessie-including-pi-3-4/).  