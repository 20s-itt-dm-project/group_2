Installing the workbench:

- Just open VSCode and go to the tab with the 4 blocks,
and search for the workbench extension made by particle.

- Once that found just install it.

Quick-Start

1. View Available Commands: CTRL+SHIFT+P (Windows and Linux) or CMD+SHIFT+P (macOS) then type Particle
2. Run the Particle: Create Project command and follow the prompts
3. Need Help? Run the Particle: Get Help command