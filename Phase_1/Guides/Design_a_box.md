Desing a Box:

1. Download necesary programs  
	- [OpenSCAD](https://www.openscad.org/downloads.html).  
	- [InkScape](https://inkscape.org/release/inkscape-0.92.4/).  
	- If not on a Mac or Linux download a Linux cmd; Help can be found [here](https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/).
2. Install Libary for OpenSCAD
	- Download it from this [github](https://github.com/bmsleight/lasercut).
	- Download it as a .zip file.
	- Inside the .zip file there should be a folder called "lasercut-master"
	- To install and use it simply drag that folder into the libary folder for OpenSCAD, which can be found when opening OpenSCAD, by pressing; File(Top left conor) -> Show Libary Folder(Near the bottom)
	- To actually use it all you have to do is say "include <lasercut-master/lasercut.scad>;"
3. Designing the Box
	- For help look at the [github](https://github.com/bmsleight/lasercut).
	- Or simply input this [code](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/3D%20And%202D%20Schematics/Prototype_for_Box.scad).
	- If it works it should look like this [box](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/3D%20And%202D%20Schematics/Prototype_for_Box.stl).
4. Make Design 2D
	- Go to the Libary Folder once again and open the lasercut-master folder  
	- There should be a file called convert-2d.sh; open it.  
	- You will need to modify it just a bit. within the first function; openscad_bin() add the path to where you installed OpenSCAD eg.
		- /mnt/c/Program\ Files/OpenSCAD/OpenSCAD.exe $1
		- This should be added at the end of the function just beneeth **else**.  
	- Open the Linux cmd within the correct folder where you have saved the design and write;
		- bash Path_to_script(libaries/lasercut-master)/convert-2d.sh Name_Of_File.scad
	- once the script has run you should now have a file called Name_Of_File.scad_2d.scad
	- You might run into an error where you have to specify the path to libary so like in step 2 just add the "lasercut-master"
	- You might also run into an error where it still does not work, if so check the code and see if there are any <**"**> if so delete them all and you should be good to go.
5. Edit 2D in InkScape
	- First you must build it in OpenSCAD to do so simply press F6
	- Once done export the file to an .DXF
	- When the file is opened in InkScape press shift+crtl+d. This opens the documents properties these must be set in mm and to the correct values for this project it is W:600 H:450.
	- Now group each piece by simply selecting the piece with your mouse and press crtl+g
	- Place the pieces nicely in withon the square
	- Select all the pieces and press shift+crtl+f to change the Stroke Paint
		- for this project we will change the outline color to red #255 for the laser printer
		- We will also change the Stroke Style and set it to 0.01mm  

If more help is needed try wacthing this [video](https://www.youtube.com/watch?v=INyVtZfIVvE&t=508s)(Note that it is all in Danish)
	
	
	
