**Unit Testing**

1. Follow the guide on this [website](https://eal-itt.gitlab.io/iot-with-particle/docs/particle/unittest/).
2. Once you have installed the VM and opened it.
    - sign in using the username: sysuser and the password: sysuser123
    - Look at the readme.md file for extra help
3. Create a new .cpp and a .h file with the same name.
    - You can make your function to convert ADC to mAmps or simply copy paste the code below
	- In the .h file copy in this [code](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Coding/ADC_test.h)
	- In the .cpp file copy in this [code](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Coding/ADC_test.cpp)
4. Open the build.sh file and replace the CBasicMath.cpp with the .cpp file you made
5. Open the testSuite.cpp and replace the code with [this](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Coding/testSuite.cpp)
6. Now run type in "bash build.sh"
7. Once done type in "bash run_test.sh"