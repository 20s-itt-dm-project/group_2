Boron:

Programming:

Guide to setup Boron:
https://docs.particle.io/quickstart/boron/#5-compile-your-code-amp-flash

Web IDE to program the boron:
https://build.particle.io/build/new

OS:

https://docs.particle.io/tutorials/device-os/device-os/

Particle Device OS abstracts much of the complexity away from the traditional 
firmware development experience. Some specific responsibilities of Device OS include:

- Secure communication: Ensuring that all communications between the device and the Particle cloud are authorized and encrypted
    
- Hardware abstraction: Providing a single, unified interface to the device, regardless of the underlying hardware architecture

- Application enablement: Exposing a feature-rich API that is used by developers to write applications for the device

- Over-the-air updates: Allowing rapid remote changes to code running on the device while providing resilience in poor connectivity environments

Unlike application firmware, Device OS is written and maintained primarily by the Particle team. 
This is a conscious decision meant to keep you focused on your particular use case without needing 
to understand the nuances of low-level device behaviors.

Capabelities:

Data sheet: 
https://docs.particle.io/datasheets/cellular/boron-datasheet/

Powering the boron:

For powering the Boron 2G/3G version, you'll either need a USB port that is able
support 2A current, or have the LiPo battery plugged in when powering over USB.
This is because the on-board u-blox modem can consumes up to 1.8A peak current
when operating in 2G mode. The Boron will intelligently source power from the
USB most of the time and keep the battery charged. During peak current 
requirements, the additional power will be sourced from the battery. 
This reduces the charge-discharge cycle load on the battery, thus 
improving its longevity.



