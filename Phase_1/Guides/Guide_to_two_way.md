**Guide to two way communication**

1. Make sure you have one way communication working  
You can see how to make it work [here](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Guides/UART.md).  
2. Update your Boron to run this [code](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Codeing/two_way_boron).  
3. Use this [code](https://gitlab.com/20s-itt-dm-project/group_2/-/blob/master/Coding/two_way.py) on your pi.  
4. Connect the wires like you did with one way communication.  
  
It should be working nicely if not this [link](https://community.particle.io/t/serial-tutorial/26946), might be helpful.