// May need to adjust location of <lasercut.scad> 
use <lasercut-master/lasercut.scad>	;
$fn=60;
projection(cut = false)

lasercutout(thickness = 3, 
          points = [[0, 0], [55, 0], [55, 200], [0, 200], [0, 0]]
          , bumpy_finger_joints = [[0, 1, 4], [180, 1, 4], [90, 1, 4], [270, 2, 4]]
        ) 

lasercutout(thickness = 3, 
          points = [[0, 0], [55, 0], [55, 200], [0, 200], [0, 0]]
        , finger_joints = [[0, 1, 4], [180, 1, 4], [90, 2, 4], [270, 1, 4]]
        , circles_remove = [[15, 27.5, 66.6667], [15, 27.5, 100]]
        ) 

lasercutout(thickness = 3, 
          points = [[0, 0], [55, 0], [55, 55], [0, 55], [0, 0]]
        , finger_joints = [[0, 2, 4], [180, 1, 4], [90, 1, 4], [270, 1, 4]]
        , circles_remove = [[10, 27.5, 22.2222]]
        ) 

lasercutout(thickness = 3, 
          points = [[0, 0], [55, 0], [55, 55], [0, 55], [0, 0]]
        , finger_joints = [[0, 1, 4], [180, 2, 4], [90, 1, 4], [270, 1, 4]]
        ) 

lasercutout(thickness = 3, 
          points = [[0, 0], [55, 0], [55, 200], [0, 200], [0, 0]]
        , bumpy_finger_joints = [[0, 1, 4], [180, 2, 4], [90, 1, 4], [270, 1, 4]]
        ) 

lasercutout(thickness = 3, 
          points = [[0, 0], [55, 0], [55, 200], [0, 200], [0, 0]]
        , bumpy_finger_joints = [[0, 1, 4], [180, 2, 4], [90, 1, 4], [270, 1, 4]]
        ) 

;
