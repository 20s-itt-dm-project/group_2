include <lasercut-master/lasercut.scad>; 

thickness = 3;
x = 55;
y = 200;

// Buttom
lasercutoutSquare(thickness=thickness, x=x, y=y,
    bumpy_finger_joints=[
                [UP, 1, 4],
                [DOWN, 1, 4],
                [LEFT,1 ,4],
                [RIGHT, 2, 4]
        ]
    );
    
// Top(lit)
translate([0,0,75])
    lasercutoutSquare(thickness=thickness, x=x, y=y,
        finger_joints=[
                [UP, 1, 4],
                [DOWN, 1, 4],
                [LEFT,2 ,4],
                [RIGHT, 1, 4]
                ],
        circles_remove=[
            [15, x/2, y/3],
            [15, x/2, y/2], // Holes to remove lit
            ]
        );

// Short Side Right
translate([0,y+10,thickness]) rotate([90,0,0]) 
    lasercutoutSquare(thickness=thickness, x=x, y=x,
        finger_joints=[
                [UP, 2, 4],
                [DOWN, 1, 4],
                [LEFT,1 ,4],
                [RIGHT, 1, 4]
                ],
        circles_remove=[
            [10, x/2, y/9]// Holes for wires
            ]
    );
    
// Short Side Left
translate([0,-10,thickness]) rotate([90,0,0]) 
    lasercutoutSquare(thickness=thickness, x=x, y=x,
        finger_joints=[
                [UP, 1, 4],
                [DOWN, 2, 4],
                [LEFT,1 ,4],
                [RIGHT, 1, 4]
            ]
    );
    
// Long side Right
translate([-10,0,75]) rotate([0,90,0])
    lasercutoutSquare(thickness=thickness, x=x, y=y,
        bumpy_finger_joints=[
                    [UP, 1, 4],
                    [DOWN, 2, 4],
                    [LEFT,1 ,4],
                    [RIGHT, 1, 4]
            ]
        );
// Long side Left
translate([75,0,75]) rotate([0,90,0])
    lasercutoutSquare(thickness=thickness, x=x, y=y,
        bumpy_finger_joints=[
                    [UP, 1, 4],
                    [DOWN, 2, 4],
                    [LEFT,1 ,4],
                    [RIGHT, 1, 4]
            ]
        );
