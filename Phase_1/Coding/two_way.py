#!/usr/bin/env python3

"""
Setup:
Pi TX (1) is connected to Boron RX
Pi RX (0) is connected to Boron TX
Pi GND is connected to Boron GND
"""

import serial
import time

serial_cfg = {  "dev": "/dev/ttyS0",
                "baud": 9600 }

count = 0

if __name__ == "__main__":
    print( "Running port {}".format( serial_cfg['dev'] ) )
    with serial.Serial(serial_cfg['dev'], serial_cfg['baud']) as ser:
	while True:
				try:
            
					print( "sending: {}".format(count))
					ser.write( "{}\r\n".format(count).encode())
					count = count +1
					time.sleep(1)
					
					reply = ser.readline().decode()   # read a '\n' terminated line
					if len( reply ) > 0:
						print( "Reply from Boron {}".format(reply.strip()))
					else:
						print( "- nothing received. Serial timeout?")
					
				except KeyboardInterrupt:
					print('interrupted!')

