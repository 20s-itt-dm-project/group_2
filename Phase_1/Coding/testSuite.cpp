#include <CppUTest/TestHarness.h>
#include "ADC_test.h"//The name of your .h file

TEST_GROUP(testSuite)
{
	ADC_test* mTestObj;
	
	void setup()
	{
	mTestObj = new ADC_test();
	mTestObj->adc_Value(4096);
	}
	
	void teardown()
	{
	delete mTestObj;
	}

};

TEST(testSuite,testAdc_value)
{
	CHECK_EQUAL(22,mTestObj->adc_Value(4096));
}

