// Constants
const unsigned long SEND_INTERVAL_MS = 1000;
const size_t READ_BUF_SIZE = 64;

// Forward declarations
void processBuffer();

// Global variables
#define button  2  // button signal is connected to Boron digital pin 2(D2)
#define LED     3  // LED is connected to Boron digital pin 3(D3)
#define ADCPIN 19 // adc pin A0

#define VOLTAGE 3.3
#define MAXVALUE 4096
#define MAXAMP 22

#define ADCSAMPLERATE 750

unsigned long lastSend = 0;
int buttonState = 0;
int ledState = 0;
int value = 0;//to store read data

int readOut[ADCSAMPLERATE];
float prevSend = 0;

char readBuf[READ_BUF_SIZE];
size_t readBufOffset = 0;


void setup() {
    pinMode(button, INPUT);  // configure button pin as input
    pinMode(LED, OUTPUT);    // configure LED pin as output
    Serial.begin(9600);
	// Serial1 RX is connected to Pi TX (1)
	// Serial2 TX is connected to Pi RX (0)
	// Photon GND is connected to Pi GND
    Serial1.begin(9600);

}

void loop() {
    buttonState = digitalRead(button);
    ledState = digitalRead(LED);
    
    for (int i = 0; i < ADCSAMPLERATE; i++)
    {
        readOut[i] = analogRead(ADCPIN);
    }

    float value = average(readOut, ADCSAMPLERATE);
    float percent = (float)value / MAXVALUE;
    float data = (percent * MAXAMP);

    if (buttonState == HIGH) {
    digitalWrite(LED, HIGH);
    }
    
    else {
    digitalWrite(LED, LOW);
    }
    
    if (millis() - lastSend >= SEND_INTERVAL_MS) {
		lastSend = millis();
        Serial1.printlnf(String(data) + "," + String(buttonState));
    }

    while (Serial.available()) {
    String s = Serial1.readStringUntil('\n');
    Serial1.printlnf("received data %s", s);
 	}
}


float average(int a[], int n){
    int sum = 0;
    for (int i = 0; i < n; i++)
    {
        sum += a[i];
    }
    return sum / n;
}

