#include "ADC_test.h"

float ADC_test::adc_Value(float value)
{
	float MAXVALUE = 4096;
	float MAXAMP = 22;
	
	float percent = (value/MAXVALUE);
	
	return (percent*MAXAMP);
}
