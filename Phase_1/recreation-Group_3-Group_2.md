Recreation checklist for group group_3
===================================================

Group name: Group_2

Checklist
------------

- [X] Project plan completed
- [ ] Minimal circuit schematic for all modules completed
- [ ] Boron minimal system code with 4-20 mA module working
- [X] Boron to Rasperry Pi serial communication over UART working
- [ ] Raspberry pi configuration documented
- [X] Raspberry pi and node red setup 
- [ ] Node red showing 4-20 mA
- [ ] Boron toolchain setup described and working


Comments
- Please convert the pdf files to .md files. 
- Documentation is dificult to navigate through.
- There is a lot empty links, in the wiki. 
- There is a lot of projects that are started, but they are not done.  



