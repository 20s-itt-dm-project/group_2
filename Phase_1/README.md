# Welcome to group 2 Phase_1 readme project (20S ITT2 Project)
---
This readme is about the project for 2nd semester students on UCL IT-teknology. 
We will work with different projects, initated by Hydac A/S. 

# Overview
---
The main goal is to have a system where sensor data is sent to a cloud server. 
Data presented and collected and control signal may be sent back.

For more information and guides to set it up please look at the [Wiki](https://gitlab.com/20s-itt-dm-project/group_2/-/wikis/Group-2-Wiki).  

![overview](https://gitlab.com/20s-itt-dm-project/group_2/-/raw/master/Phase_1/Research%20and%20Documentation/docs_photos_project_overview_no_mesh.png?inline=false)

See the link for visual overview.  
  

# The main goals of our project 

* DUT: Device under test, these are the pumps from Hydac

* Hydac sensor: These are sensor of different kinds that we are to implement into the system. Initially we will work with physical sensor simulations (analog/digital).  

* Particle Boron: The embedded system to run the sensor software and to be the interface to the serial connection to the raspberry pi and the gateway to particle device cloud.  

* Computer: Hosts the particle workbench IDE (Visual studio code)  

* Raspberry Pi: A small hardened linux system hosting a dashboard and relevant programs to upload/download data from the Boron and to/from the APIs. Configuration must be reproducable with appropriate security implemented.

# Particle Boron 

Link overview of the Particle Boron, with pinout. 

![Boron_pin_overview](https://gitlab.com/20s-itt-dm-project/group_2/-/raw/master/Phase_1/Guides/Boron_pin_overview.png)

# Organization
  
    • Identification of the steering committee
	- Jacob
	- Jonas
	- Mikkel
	- Thomas  
    • Identification of project managers
	- Nikolaj
	- Morten
    • Composition of the project groups
	- IT-Teknology
	- Computer sience
    • Identification of external resource persons or groups  
	- Hydac A/S

