Recreation checklist for group group_4
===================================================

Group name: Group_2

Checklist
------------

- [X] Project plan completed
- [X] Minimal circuit schematic for all modules completed
- [ ] Boron minimal system code with 4-20 mA module working
- [X] Boron to Rasperry Pi serial communication over UART working
- [ ] Raspberry pi configuration documented
- [ ] Raspberry pi and node red setup 
- [ ] Node red showing 4-20 mA
- [ ] Boron toolchain setup described and working


Comments:
- Documentation, is hard to figure out. 
- Missing system code for 4-20mA module (Borron)
- Missing Raspberry pi configuration
- Missing noted red setup on Raspberrypi 





